-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Pizzaria
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Seguradora` ;

-- -----------------------------------------------------
-- Schema Pizzaria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Seguradora` ;
-- -----------------------------------------------------
-- Schema Seguradora
-- -----------------------------------------------------
USE `Seguradora` ;

-- -----------------------------------------------------
-- Table `Pizzaria`.`clienteSeguradora`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Seguradora`.`clienteSeguradora` ;

CREATE TABLE IF NOT EXISTS `Seguradora`.`clienteSeguradora` (
  `cpf` VARCHAR(14) NOT NULL,
  `nome` VARCHAR(45) NULL,
  `nasc` DATE NULL,
  `genero` VARCHAR(45) NULL,
  PRIMARY KEY (`cpf`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pizzaria`.`apolice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Seguradora`.`apolice` ;

CREATE TABLE IF NOT EXISTS `Seguradora`.`apolice` (
  `numero` INT NOT NULL,
  `dataInicio` DATE NULL,
  `dataTermino` DATE NULL,
  `valor` FLOAT NULL,
  `Franquia` VARCHAR(45) NULL,
  `cliente_cpf` VARCHAR(14) NOT NULL,
  PRIMARY KEY (`numero`, `cliente_cpf`),
  INDEX `fk_apolice_cliente1_idx` (`cliente_cpf` ASC),
  CONSTRAINT `fk_apolice_cliente1`
    FOREIGN KEY (`cliente_cpf`)
    REFERENCES `Seguradora`.`clienteSeguradora` (`cpf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pizzaria`.`carro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Seguradora`.`carro` ;

CREATE TABLE IF NOT EXISTS `Seguradora`.`carro` (
  `chassis` VARCHAR(14) NOT NULL,
  `modelo` VARCHAR(45) NULL,
  `ano` DATE NULL,
  `apolice_numero` INT NOT NULL,
  PRIMARY KEY (`chassis`, `apolice_numero`),
  INDEX `fk_carro_apolice1_idx` (`apolice_numero` ASC),
  CONSTRAINT `fk_carro_apolice1`
    FOREIGN KEY (`apolice_numero`)
    REFERENCES `Seguradora`.`apolice` (`numero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pizzaria`.`acidente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Seguradora`.`acidente` ;

CREATE TABLE IF NOT EXISTS `Seguradora`.`acidente` (
  `numBO` VARCHAR(14) NOT NULL,
  `dataHora` DATETIME NULL,
  `Local` VARCHAR(45) NULL,
  `carro_chassis` VARCHAR(14) NOT NULL,
  PRIMARY KEY (`numBO`, `carro_chassis`),
  INDEX `fk_acidente_carro1_idx` (`carro_chassis` ASC),
  CONSTRAINT `fk_acidente_carro1`
    FOREIGN KEY (`carro_chassis`)
    REFERENCES `Seguradora`.`carro` (`chassis`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
