INSERT INTO `Pizzaria`.`Pizza` (`nome`, `preco`, `descricao`, `tamanho`) VALUES ('Calabresa', '29.90', 'Finas e suculentas fatias de calabresa artesanal', 'Grande');
INSERT INTO `Pizzaria`.`Pizza` (`nome`, `preco`, `descricao`, `tamanho`) VALUES ('Muçarela', '29.90', 'Lascas cremosas de queijo muçarela de leite holandês', 'Grande');
INSERT INTO `Pizzaria`.`Pizza` (`nome`, `preco`, `descricao`, `tamanho`) VALUES ('Brigadeiro', '19.90', 'Chocolate belga com granulado', 'Brotinho');
