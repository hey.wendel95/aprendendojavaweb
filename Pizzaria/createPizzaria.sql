-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Pizzaria
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Pizzaria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Pizzaria` ;
USE `Pizzaria` ;

-- -----------------------------------------------------
-- Table `Pizzaria`.`Pizza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`Pizza` (
  `nome` VARCHAR(100) NOT NULL,
  `preco` DECIMAL(4,2) NULL,
  `descricao` VARCHAR(300) NULL,
  `tamanho` VARCHAR(45) NULL,
  PRIMARY KEY (`nome`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agencia`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`cliente` (
  `cpf` VARCHAR(16) NOT NULL,
  `nome` VARCHAR(45) NULL,
  `endereco` VARCHAR(45) NULL,
  `telefone` VARCHAR(45) NULL,
  `Pizza_nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`cpf`),
  INDEX `fk_cliente_Pizza_idx` (`Pizza_nome` ASC),
  CONSTRAINT `fk_cliente_Pizza`
    FOREIGN KEY (`Pizza_nome`)
    REFERENCES `Pizzaria`.`Pizza` (`nome`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agencia`.`Pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`Pedido` (
  `codigo` INT NOT NULL,
  `data` TIMESTAMP NULL,
  `formaPagto` VARCHAR(45) NULL,
  `valorTotal` DECIMAL(4,2) NULL,
  `cliente_cpf` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Pedido_cliente1_idx` (`cliente_cpf` ASC),
  CONSTRAINT `fk_Pedido_cliente1`
    FOREIGN KEY (`cliente_cpf`)
    REFERENCES `Pizzaria`.`cliente` (`cpf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agencia`.`Ingrediente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`Ingrediente` (
  `codigo` INT NOT NULL,
  `nome` VARCHAR(45) NULL,
  `estoque` INT NULL,
  `validade` DATE NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agencia`.`Ingrediente_has_Pizza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`Ingrediente_has_Pizza` (
  `Ingrediente_codigo` INT NOT NULL,
  `Pizza_nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Ingrediente_codigo`, `Pizza_nome`),
  INDEX `fk_Ingrediente_has_Pizza_Pizza1_idx` (`Pizza_nome` ASC),
  INDEX `fk_Ingrediente_has_Pizza_Ingrediente1_idx` (`Ingrediente_codigo` ASC),
  CONSTRAINT `fk_Ingrediente_has_Pizza_Ingrediente1`
    FOREIGN KEY (`Ingrediente_codigo`)
    REFERENCES `Pizzaria`.`Ingrediente` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ingrediente_has_Pizza_Pizza1`
    FOREIGN KEY (`Pizza_nome`)
    REFERENCES `Pizzaria`.`Pizza` (`nome`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agencia`.`Pedido_has_Pizza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pizzaria`.`Pedido_has_Pizza` (
  `Pedido_codigo` INT NOT NULL,
  `Pizza_nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Pedido_codigo`, `Pizza_nome`),
  INDEX `fk_Pedido_has_Pizza_Pizza1_idx` (`Pizza_nome` ASC),
  INDEX `fk_Pedido_has_Pizza_Pedido1_idx` (`Pedido_codigo` ASC),
  CONSTRAINT `fk_Pedido_has_Pizza_Pedido1`
    FOREIGN KEY (`Pedido_codigo`)
    REFERENCES `Pizzaria`.`Pedido` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedido_has_Pizza_Pizza1`
    FOREIGN KEY (`Pizza_nome`)
    REFERENCES `Pizzaria`.`Pizza` (`nome`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
